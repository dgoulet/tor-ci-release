#!/bin/bash

# Load some useful functions and variables.
source ./util.sh

# Get Tor repository
clone_tor --depth=1 -b main

# Get fallbackdir scripts repository
runcmd git clone --depth=1 -b main https://gitlab.torproject.org/tpo/core/fallback-scripts.git

# Build the list
runcmd cargo run --manifest-path fallback-scripts/Cargo.toml --release

# Copy the new lists in Tor.
runcmd cp -f tor-git_fallback_dirs.inc tor/src/app/config/fallback_dirs.inc

# Get in Tor, create changes file and commit.
runcmd cd tor/

DASH_DATE=$(date -u +"%Y-%m-%d")
CHANGES_DATE=$(date -u +"%B %d, %Y")
CHANGESFILE="changes/fallbackdirs-$DASH_DATE"

cat > "$CHANGESFILE" <<EOF
  o Minor features (fallbackdir):
    - Regenerate fallback directories generated on ${CHANGES_DATE}.
EOF

# Add and commit the changes.
runcmd git add "$CHANGESFILE"
runcmd git commit -a -m "fallbackdir: Update list generated on ${CHANGES_DATE}"

# Extract the patch and put it in the artifacts. This applies to all
# maintained branch and thus why we put it in "maint". It is merge forward.
runcmd git format-patch -1 -o "$PATCHES_DIR/maint"

# Go back outside of tor repository.
runcmd cd ../

# Get arti repository to build the patch.
clone_arti --depth=1 -b main

# Apply patch
runcmd cp -f tor-arti_fallback_dirs.inc ./arti/crates/tor-dirmgr/src/fallback_dirs.inc
runcmd cd arti/

# Add and commit the changes.
runcmd git add -u
runcmd git commit -s -m "fallbackdir: Update list generated on ${CHANGES_DATE}"

# Extract the patch and put it in the artifacts for arti.
runcmd git format-patch -1 -o "$PATCHES_DIR/arti"
