#!/bin/bash

# Git release branch name.
RELEASE_BRANCH_NAME="release"
# Defaults
VERSIONS=${VERSIONS:-}

# Load some useful functions and variables.
source ./util.sh

# Make sure we do have versions before anything.
if [ -z "$VERSIONS" ]; then
    die "Missing variables VERSIONS. Please edit versions.yml."
fi

# Cloning tor so we can use the list tor branches script that will give us the
# maintained branches.
clone_tor

# Get into tor repository
runcmd cd tor/

# For each defined versions, find the branch and create an artifacts where the
# filename is the version and the content will be the branch to use.
for version in $VERSIONS
do
    # Basic check on tor.git version format. The following dies if bad.
    validate_version_format "$version"

    # Extract major version from given value.
    MAJOR_VERSION="$(major_version "$version")"
    # For releases, we only check at the release- branch or main if an
    # alpha/rc. If we can't find the release- branch, we'll try to see if main
    # is the right branch.
    BRANCH="$RELEASE_BRANCH_NAME-$MAJOR_VERSION"

    # Validate that such a branch exists.
    if ! git rev-parse --verify "remotes/origin/$BRANCH"; then
        # It could be "main" as in an alpha/rc so check the configure.ac.
        AC_INIT=$(grep AC_INIT configure.ac)
        if [[ ! "$AC_INIT" == *"$MAJOR_VERSION"* ]]; then
            die "Branch $BRANCH not found in tor.git repository nor can main be used."
        fi
        BRANCH="main"
    fi
    success "Found branch $BRANCH for $version"

    # Lets validate that we actually have changes and if not, abort pipeline
    # so we can notice that what VERSIONS contains is not workable.
    runcmd git switch "$BRANCH"

    num_file=$(find changes/ -type f | wc -l)
    if [ "$num_file" -eq 1 ] && [ -f "changes/.dummy" ]; then
        die "No changes file for $version in branch $BRANCH. Stopping."
    fi

    # Write the artifacts for the build stage
    echo "$BRANCH" > "$VERSIONS_DIR/$version"
done
